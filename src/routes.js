import React from 'react';
import { Route, IndexRoute } from 'react-router';

// import HomePage from './components/HomePage';
// import FuelSavingsPage from './containers/FuelSavingsPage';
// import AboutPage from './components/AboutPage';
// import NotFoundPage from './components/NotFoundPage';
import App from './components/App';
import Signin from './components/Signin';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Signin}/>
  </Route>
);
